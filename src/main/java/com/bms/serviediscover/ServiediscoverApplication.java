package com.bms.serviediscover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ServiediscoverApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiediscoverApplication.class, args);
	}

}
